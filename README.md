# Algorytm genetyczny 
## Opis
W projekcie wykorzystany został framework Spring Python do symulacji algorytmu genetycznego.
System składa się z oddzielnych komponentów, które są odpowiedzialne za:
* Ewaluacje
* Mutacje
* Selekcje
* Krzyzowanie

## Konfiguracja 
W pliku GeneticComponents.py istnieje możliwośc zdefiniowania poniższych zmiennych:
* function - funkcja zddefiniowana jako implementacja klasy Function
* population_size - rozmiar populacji
* diff_const - wspolczynnik roznicy sredniej funkcji dostosowania pomiedzy generacjami wymagany do zakonczenia algorytmu
* ournamentSelectionSize - ilosc osobnikow w grupie turniejowej
* pc - wspolczynnik krzyzowania
* prob - prawdopodobienstwo mutacji
* pm - wspolczynnik mutacji